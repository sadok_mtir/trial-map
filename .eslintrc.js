module.exports = {
  'extends': 'airbnb',
  'rules': {
    'linebreak-style': 0,
  },
  'plugins': [
    'react',
    'jsx-a11y',
    'import'
  ],
  'env': {
    'jasmine': true,
    'jest': true,
    'browser': true,
    'node': true
  }
};