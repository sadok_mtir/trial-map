/**
 * @description The function returns the AppProvider
 * @param frontendId that was passed by the user which frontend should be shown
 * @returns {*}
 * @constructor
 */
import React from 'react';
// import { Provider } from 'react-redux';
// import { PersistGate } from 'redux-persist/lib/integration/react';

import App from './componnets/index';
// import store, { persistor } from './config/store';
// import './styles/global.css';
// import './styles/fontello.css';

const AppProvider = () => (
  <App />
);

export default AppProvider;
