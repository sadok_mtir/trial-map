import ReactDOM from 'react-dom';
import AppProvider from './provider';
import registerServiceWorker from './registerServiceWorker';

const ROOT_NODE = document.getElementById('root');

const render = (element) => {
  ReactDOM.render(AppProvider(), element);
};

render(ROOT_NODE);
registerServiceWorker();
