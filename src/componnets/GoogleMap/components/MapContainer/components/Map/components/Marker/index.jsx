import { Component } from 'react';
import PropTypes from 'prop-types';

export class Marker extends Component {

  componentDidUpdate(prevProps) {
    if ((this.props.map !== prevProps.map) ||
      (this.props.position !== prevProps.position)) {
      this.renderMarker();
    }
  }

  componentDidMount() {
    if (this.props.map) this.renderMarker();
  }

  componentWillUnmount() {
    if (this.marker) {
      this.marker.setMap(null);
    }
  }

  renderMarker() {
    const { map, google } = this.props;

    const pos = this.props.position;
    const position = new google.maps.LatLng(pos.lat, pos.lng);

    const pref = {
      map,
      position,
    };
    this.marker = new google.maps.Marker(pref);
  }

  render() {
    return null;
  }
}

Marker.propTypes = {
  position: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  map: PropTypes.shape(),
  google: PropTypes.shape(),
};

Marker.defaultProps = {
  position: null,
  map: null,
  google: null,
};
