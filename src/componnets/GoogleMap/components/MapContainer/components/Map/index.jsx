import React, { Component } from 'react';
import PropTypes from 'prop-types';

const style = {
  width: '100vw',
  height: '100vh',
};
export default class Map extends Component {
  constructor(props) {
    super(props);
    this.containerRef = null;
    this.state = {
      currentLocation: { ...props.initialCenter },
      map: null,
    };
  }

  componentDidMount() {
    this.loadMap();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.google !== this.props.google) {
      this.loadMap();
    }
  }

  loadMap() {
    if (this.props && this.props.google) {
      // google is available
      const { google } = this.props;
      const { maps } = google;
      const node = this.containerRef;
      const zoom = 13;
      const { lat, lng } = this.state.currentLocation;
      const center = new maps.LatLng(lat, lng);
      const mapConfig = Object.assign({}, {
        center,
        zoom,
      });
      this.setState({
        map: new maps.Map(node, mapConfig),
      });
    }
  }

  renderChildren() {
    const { children } = this.props;

      debugger

    const markers = React.Children.map(children, c => React.cloneElement(c, {
      map: this.state.map,
      google: this.props.google,
      position: c.props.position || this.state.currentLocation,
    }));
    return markers || null;
  }


  render() {
    return (
      <div
        ref={(e) => {
          this.containerRef = e;
        }}
        style={style}
      >
        Loading map...
        {this.renderChildren()}
      </div>);
  }
}

Map.propTypes = {
  google: PropTypes.shape(),
  zoom: PropTypes.number,
  initialCenter: PropTypes.shape(),
};
Map.defaultProps = {
  google: null,
  zoom: 13,
  initialCenter: {
    lat: 51.2876146,
    lng: 6.7667911999999,
  },
};

