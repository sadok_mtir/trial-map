import React, { Component } from 'react';
import { GoogleApiWrapper } from 'google-maps-react';
import PropTypes from 'prop-types';
import Map from './components/Map';
import { Marker } from './components/Map/components/Marker/index';

class MapContainer extends Component {
  render() {
    return (
      <Map google={this.props.google}>
        <Marker />
      </Map>
    );
  }
}
MapContainer.propTypes = {
  google: PropTypes.shape(),
};
MapContainer.defaultProps = {
  google: null,
};

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAj8CdW5s3UH7KY3sdwodu8btK-UZNYbzU',
})(MapContainer);
